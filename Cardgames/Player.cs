﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AllanTech.CardGameLib
{
    public abstract class Player
    {
        public virtual string Name { get; protected set; }

        protected CardCollection hand;


        /// <summary>
        /// Called when a new game is started
        /// </summary>
        /// <param name="g"></param>
        public virtual void PlayerNewGameInit(IGame g)
        {

        }

        /// <summary>
        /// Called whenever it's the players turn
        /// </summary>
        /// <param name="g"></param>
        public virtual void PlayerTurn(IGame g)
        {

        }

        public virtual void DealCards(IEnumerable<Card> cards)
        {
            foreach (var c in cards) DealCard(c);
        }

        public virtual void DealCard(Card c){
            hand.Add(c);
        }

        public Player(string Name)
        {
            this.Name = Name;
            hand = new CardCollection();
        }
    }
}
