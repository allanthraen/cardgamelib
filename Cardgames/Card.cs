﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AllanTech.CardGameLib
{
    /// <summary>
    /// Represents a playing card
    /// </summary>
    public class Card : IComparable<Card>, IEquatable<Card>
    {
        public CardSuits Suit { get; private set; }
        public CardValues Value { get; private set; }

        public CardCollection CurrentLocation { get; set; }
        public object Tag { get; set; }

        public bool IsRed
        {
            get
            {
                return !IsBlack;
            }
        }

        public bool IsBlack
        {
            get
            {
                return (IntSuit % 2) == 0;
            }
        }

        public bool IsJoker
        {
            get
            {
                return (Value == CardValues.Joker);
            }
        }

        public int IntValue
        {
            get
            {
                return (int)Value;
            }
        }

        public int IntSuit
        {
            get
            {
                return (int)Suit;
            }
        }

        public static CardCollection operator +(Card a, Card b)
        {
            return new CardCollection(new Card[] { a, b });
        }


        public override string ToString()
        {
            return string.Format("{0} of {1}", Value, Suit);
        }

        public static Card Deserialize(string c)
        {
            char v = c[1];
            char s = c[0];
            CardValues cv = (CardValues)int.Parse(v.ToString(), System.Globalization.NumberStyles.HexNumber);
            CardSuits cs = (CardSuits)int.Parse(s.ToString());
            return new Card(cs, cv);
        }

        

        public virtual string Serialize()
        {
            return ((int)Suit).ToString() + ((int)Value).ToString("X");
        }


        public virtual string ToShortString()
        {
            string v;
            switch (Value)
            {
                case CardValues.Ace: v = "A"; break;
                case CardValues.Ten: v = "T"; break;
                case CardValues.Jack: v = "J"; break;
                case CardValues.Queen: v = "Q"; break;
                case CardValues.King: v = "K"; break;
                case CardValues.Joker: v = "?"; break;
                default: v = ((int)Value).ToString(); break;
            }
            string s = Suit.ToString().Substring(0, 1);
            return v + s;
        }

        public Card(CardSuits Suit, CardValues Value)
        {
            this.Suit = Suit;
            this.Value = Value;
        }


        #region IComparable<Card> Members

        public int CompareTo(Card other)
        {
            return this.IntValue.CompareTo(other.IntValue);
        }

        #endregion

        #region IEquatable<Card> Members

        public bool Equals(Card other)
        {
            if (other == null) return false;
            return (other.Suit == this.Suit) && (other.Value == this.Value);
        }

        #endregion
    }
}
