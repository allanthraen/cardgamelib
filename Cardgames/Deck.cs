﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AllanTech.CardGameLib
{
    //Save-load logic
    public class Deck : CardCollection
    {
        protected Random rand;

        public virtual void Shuffle()
        {
            for (int i = 0; i < 50000; i++)
            {
                int a = rand.Next(0, this.Count);
                int b = rand.Next(0, this.Count);
                if (a != b)
                {
                    Card c = this[a];
                    this[a] = this[b];
                    this[b] = c;
                }
            }
        }

        /// <summary>
        /// Create cards
        /// </summary>
        /// <param name="Jokers">Number of Jokers</param>
        protected virtual void Initialize(int Jokers)
        {
            int jok = 0;
            for (int s = 1; s <= 4;s++ )
            {
                for (int v = 1; v <= 13; v++)
                {
                    this.Add(new Card((CardSuits)s, (CardValues)v));
                }
                if (jok < Jokers)
                {
                    this.Add(new Card((CardSuits)s, CardValues.Joker));
                    jok++;
                }
            }
        }

        public Deck()
        {
            rand = new Random();
        }

        public Deck(int Jokers) : this()
        {
            Initialize(Jokers); 
        }
    }
}
