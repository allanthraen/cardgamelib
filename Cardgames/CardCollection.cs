﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AllanTech.CardGameLib
{
    /// <summary>
    /// A Collection of Cards
    /// </summary>
    public class CardCollection : List<Card>, ICloneable
    {

        public virtual new void Add(Card c)
        {
            c.CurrentLocation = this;
            base.Add(c);
        }

        public virtual new void Remove(Card c)
        {
            c.CurrentLocation = null;
            base.Remove(c);
        }



        public virtual CardCollection Draw(int count)
        {
            CardCollection cc = new CardCollection();
            for (int i = 0; i < count; i++)
            {
                Card c = this.Draw();
                if (c == null) break;
                cc.Add(c);
            }
            return cc;
        }

        public virtual Card Draw()
        {
            if (this.Count == 0) return null;
            Card c = this[0];
            this.RemoveAt(0);
            return c;
        }

        public virtual Card DrawLastCard()
        {
            if (this.Count == 0) return null;
            Card c = this[this.Count - 1];
            this.RemoveAt(this.Count - 1);
            return c;
        }

        

        public Card FirstCard
        {
            get
            {
                if (this.Count == 0) return null;
                return this[0];
            }
        }

        public Card LastCard
        {
            get
            {
                if (this.Count == 0) return null;
                return this[this.Count - 1];
            }
        }

        public override string ToString()
        {
            return string.Join("\t", this.Select(c => c.ToShortString()).ToArray());
        }

        public Card[] TakeLastCards(int count)
        {
            var c = this.GetRange(this.Count - 1 - count, count);
            c.ForEach(cc => this.Remove(cc));
            return c.ToArray();
        }

        public static CardCollection operator +(CardCollection a, Card b)
        {
            a.Add(b);
            return a;
        }

        public virtual string SerializeToString()
        {
            return string.Join("|", this.Select(c => c.Serialize()).ToArray());
        }

        public virtual void DeserializeString(string s)
        {
            s.Split('|').ToList().ForEach(a => this.Add(Card.Deserialize(a)));
        }


        public virtual int Sum(bool AceIsFourteen)
        {
            int sum = 0;
            this.ForEach(c => sum += (c.Value==CardValues.Ace && AceIsFourteen)? (int)c.Value:14);
            return sum;
        }

        public CardCollection()
            : base()
        {
        }

        public CardCollection(IEnumerable<Card> Cards)
            : base(Cards)
        {
        }

        #region ICloneable Members

        public object Clone()
        {
            return new CardCollection(this);
        }

        #endregion
    }
}
