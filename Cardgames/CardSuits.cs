﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AllanTech.CardGameLib
{
    public enum CardSuits
    {
        Hearts=1,
        Spades=2,
        Diamonds=3,
        Clubs=4
    }

    public enum CardValues
    {
        Joker=0,
        Ace=1,
        Two=2,
        Three=3,
        Four=4,
        Five=5,
        Six=6,
        Seven=7,
        Eight=8,
        Nine=9,
        Ten=10,
        Jack=11,
        Queen=12,
        King=13
    }
}
