﻿using System;
namespace AllanTech.CardGameLib
{
    public interface IGame
    {
        bool Started { get; }
        void StartGame();
        CardCollection Table { get; set; }
    }
}
